# uDoor
uPort Door Management

## Install

`npm install`

Create a `config.js` file (you can rename the `config.js.sample` and edit it)

## Run

`node index.js`

or

`pm2 start index.js --name udoor`
