const localtunnel = require('localtunnel');
const express = require('express'); 
const bodyParser = require("body-parser");
const Credentials=require('uport').Credentials;
const Gpio = require('onoff').Gpio;
const config = require('./config');


const SimpleSigner=require('uport').SimpleSigner;

const credentials = new Credentials({
    appName: config.appName,
    address: config.address,
    signer:  new SimpleSigner(config.privateKey)
})

let tunnelUrl;

const app = express(); 

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res)=> {
    const requestOpts={
        callbackUrl: tunnelUrl,
        //exp: Math.floor(Date.now() / 1000) + 365*24*60*60
    }
    
    credentials.createRequest(requestOpts).then((token)=>{
        res.redirect(301,'https://id.uport.me/me?requestToken='+token);
    })
});

app.post('/', (req, res) => {
    credentials.receive(req.body.access_token).then((profile)=>{
        console.log(profile);
        //openDoor();
        res.send('Abierto!') 
    }).catch((err)=>{
        console.log(err);
        res.send(err.message);
    })
}); 

let port=config.port;
app.listen(port, () => console.log('localhost on port '+port) );

const tunnelOpts={subdomain: config.subdomain};
let tunnel = localtunnel(port, tunnelOpts, function(err, tunnel) {
    if (err) console.log(err);
    tunnelUrl=tunnel.url;
    console.log("Remote URL: "+tunnel.url);
});

tunnel.on('close', function() {
    // tunnels are closed
    console.log("Tunnel closed")
});



function openDoor(){
    const rele = new Gpio(4,'out');
    rele.writeSync(1);
    setTimeout(()=>{
        rele.writeSync(0);
        rele.unexport();
        },1500);
}
